export default {
    namespaced: true,
    state: {
        blank_cards:[
            {'rank': 'A', rate:14},
            {'rank': 'K', rate:13},
            {'rank': 'Q', rate:12},
            {'rank': 'J', rate:11},
            {'rank': '10', rate:10},
            {'rank': '9', rate:9},
            {'rank': '8', rate:8},
            {'rank': '7', rate:7},
            {'rank': '6', rate:6},
            {'rank': '5', rate:5},
            {'rank': '4', rate:4},
            {'rank': '3', rate:3},
            {'rank': '2', rate:2},
        ],
        suits:['c','p','k','b'],
        logic_cards:[]
    },
    mutations: {
        createLogicCards(state){
            state.suits.forEach(suit => {
                state.blank_cards.forEach(card=>{
                    const logic_card = {suit:suit,rank:card.rank,rate:card.rate};
                    state.logic_cards.push(logic_card);
                });
            });
            //console.log('view_cards = ', state.logic_cards);
        }
    },
    actions: {
        
    },
    getters: {
        getStatus:state=>{return state.app_stop},
        getLoadStatus:state=>{return state.app_load},
        getPath:state=>{return state.path}
    }
}