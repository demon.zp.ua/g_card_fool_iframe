import scale from "../services/scaleService";

export default {
    namespaced: true,
    state: {
        pos_labels:{
            pl_1:{
                id:null,
                x:0,
                y:0,
                scale:1
            },
            pl_2:{
                id:null,
                x:0,
                y:0,
                scale:1
            },
            pl_3:{
                id:null,
                x:0,
                y:0,
                scale:1
            },
            pl_4:{
                id:null,
                x:0,
                y:0,
                scale:1
            }
        },
        game_render:null,
        test:{},
        action_queue:[],
        game:{
            user_name:'RenamedUser53221354651321',
            user_id:'dwad485wa',
            user_cards:[
                {suit:'k',rank:6},
                {suit:'c',rank:'K'},
                {suit:'k',rank:10},
                {suit:'p',rank:8},
                {suit:'c',rank:'J'},
                {suit:'b',rank:9},
                {suit:'b',rank:6},
                {suit:'p',rank:'K'},
                {suit:'b',rank:10},
                {suit:'k',rank:8}
            ],
            enemys:[
                {
                    name:'RenamedUser96221354651321',
                    id:'654dawd56',
                    deck: 6
                },
                {
                    name:'RenamedUser53221354651568',
                    id:'654dwa',
                    deck: 20
                },
                {
                    name:'RenamedUser53221354652684',
                    id:'98d4wad5',
                    deck: 36
                }
            ],
            main_deck:20,
            tramp_card:{
                suit:'c',
                rank:'7'
            },
            deck_battle:[
                {
                    podkid:{suit:'k',rank:6},
                    beating:{suit:'k',rank:'K'}
                },
                {
                    podkid:{suit:'p',rank:8},
                    beating:null
                },
                {
                    podkid:{suit:'b',rank:6},
                    beating:{suit:'k',rank:'K'}
                },
                {
                    podkid:{suit:'k',rank:9},
                    beating:{suit:'k',rank:'K'}
                },
                {
                    podkid:{suit:'c',rank:6},
                    beating:{suit:'k',rank:'K'}
                },
                {
                    podkid:{suit:'k',rank:10},
                    beating:{suit:'k',rank:'K'}
                }
            ]
        }
    },
    mutations: {
        setPosLabel(state,data){
            
            for(let player in state.pos_labels){
                if(state.pos_labels[player].id==null){
                    state.pos_labels[player] = data;
                    break;
                }else if(state.pos_labels[player].id===data.id){
                    state.pos_labels[player] = data;
                    break;
                }
            }
            console.log("state.pos_labels = ",state.pos_labels);
        },
        delPlayer(state){
            delete state.game.enemys[2];
            //console.log("state.game.enemys = ", state.game.enemys);
        }
    },
    actions: {
        onResize({state},data){
            //console.log('тут!!!');
            /*if(!state.game_render){
                return Promise.reject('game is undefined');
            }*/

            const size = scale.getSize(data.w,data.h);
            if(data.w<data.h){
                //console.log("должен повернуть!!!");
                size['rot'] = 90;
            }
            //console.log('пушу задачу!!!');
            state.action_queue.push({action:'resize',data:size});
            //state.game_render.onResize(size.w,size.h);
            //console.log("Promise.resolve.size = ", size);
            return Promise.resolve(size);
        }
    },
    getters: {
        getNumPlayers:state=>{
            return 1+state.game.enemys.length;
        },
        getNumCards:state=>{
            let num_cards = state.game.user_cards.length;
            state.game.enemys.forEach(enemy => {
                num_cards+=enemy.deck;
            });
            return num_cards;
        },
        getPosLabels:state=>{
            //console.log("в геттере!");            
            return state.pos_labels;
        },
        findLabel:state=>id=>{
            for(let pl in state.pos_labels){
                if(state.pos_labels[pl].id===id){
                    return state.pos_labels[pl];
                }
            }
            return false;
        },
        getPosLabel:(state,getters)=>id=>{
            //console.log("ищю по id = ", id);
            const cssObj = {
                position:'absolute', 
                pointerEvents:'auto',
                //minWidth:'120px',
                //maxWidth:'200px',
                wordBreak:'break-all'
            };
            const label = getters.findLabel(id);
            if(!label){
                //console.log('тут!');
                return cssObj;
            }
            

            //const pos = state.pos_labels[id];
            /*if(!pos){
                console.log('тут!');
                return cssObj;
            }*/
            //console.log("id = ", id);
            //console.log("label.x = ", label.x);
            cssObj['backgroundColor'] = '#fafafa';
            cssObj['fontSize'] = `${22*label.scale}px`;
            cssObj['minWidth']=`${160*label.scale}px`;
            cssObj['maxWidth'] = `${180*label.scale}px`;
            cssObj['left'] = `${label.w*label.proc_x/100}px`;
            cssObj['top'] = `${label.h*label.proc_y/100}px`;
            console.log('отдаю cssObj= ',cssObj);
            return cssObj;
        },
        getPlayersLabels:state=>{
            let labels = [];
            labels.push({
                id:state.game.user_id,
                name:state.game.user_name
            });
            //console.log('отдаю = ', labels);
            for(let enemy in state.game.enemys){
                labels.push(state.game.enemys[enemy]);
                //console.log('отдаю!! = ', labels);
            }
            // console.log('отдаю! = ', labels);
            return labels;
        }
    }
}