import store from '../../store';

export default class distributionService{
    constructor({scene,player,enemys}){
        this.scene = scene;
        this.i = 1;
        this.num_players = store.getters['game/getNumPlayers'];
        console.log("this.num_players = ", this.num_players);
        this.num_cards = store.getters['game/getNumCards'];
        //this.temp_num_cards = 0;
        this.player = player;
        this.enemys = enemys;
        //console.log("this.num_cards = ", this.num_cards);
        //this.d_num_cards = 0;
        //console.log('вызов distributionService!!');
        scene.time.addEvent({ delay: 300, repeat:this.num_cards-1, callback: this.distributionCard, callbackScope: this});
        //timer.time.addEvent({ delay: 800, repeat:this.num_cards, callback: this.distributionCard, callbackScope: this});
    }

    distributionCard(){
        //console.log('distributionService.distributionCard!!!');
        if(this.i>this.num_players){
            this.i=1;
        }

        /*if(this.temp_num_cards>=this.num_cards){
            this.scene.time.destroy();
        }*/

        if(this.i==1){
            //console.log('this.i==1!!!');
            this.i++;
            //this.temp_num_cards++;
            this.player.distributionCard();
            return;
        }
        
        this.i++;
        //this.temp_num_cards++;
        this.enemys.distributionCard(this.i);
        return;
    }
}