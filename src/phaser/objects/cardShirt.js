import Card from './card';

export default class CardShirt extends Card{
    constructor({scene,x,y,scale,rot}){
        arguments[0]['texture'] = 'shirt1';
        super(...arguments);
        this.rot = 0 || rot;
        this.graphics = null;
        this.graphics_shadow = null;
        this.render();
    }

    render(){
        if(this.rot===0){
            return;
        }

        this.sprite.angle = 90;
    }

    onResize(pos,scale){
        this.scale = scale;
        super.onResize();
        this.setAnimMoveTo({x:this.sprite.x, y:this.sprite.y},pos,this.sprite);
    }

    update(){
        //console.log('тут!');
        this.animate(this.sprite);
    }

    del(){
        this.sprite.destroy();
        delete this;
    }
}