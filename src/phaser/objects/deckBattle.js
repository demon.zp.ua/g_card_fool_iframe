import Deck from "./deck";
import DeckMatch from "./deckMatch";
import scaleService from "../../services/scaleService";

export default class DeckBattle extends Deck{
    constructor({scene, cards}){
        super(...arguments);
        this.x = scene.width_lvl/2;
        this.y = scene.height_lvl/2;
        this.width = 580;
        this.height = 210;
        this.x0 = this.x - this.width/2;
        this.x1 = this.x + this.width/2;
        this.y0=0;
        this.column = 1;
        this.must_add_column = false;
        //this.temp_c_column = this.col_column;
        this.pos_x = this.x0;
        //this.pos_x = this.x;
        this.pos_y = 0;
        this.step_x = 110;
        this.step_y = 100;
        this.drop_tilesprite = null;
        this.battle_matchs = {};
        this._scale = 1;
        this.scale = this._scale;
        this.render(cards);
    }

    render(cards){
        
        this.drop_tilesprite = this.scene.add.tileSprite(this.x, this.y, this.width, this.height, 'battle_field')
        .setInteractive()
        .on('pointerdown', this.onClick, this);

        for(let i=0;i<cards.length;i++){
            this._addDeckMatch(cards[i]);
        }
        document.addEventListener('custom_select', this.customSelect.bind(this));
    }

    onResize(){
        this.x = this.scene.width_lvl/2;
        this.y = this.scene.height_lvl/2.5;
        this.must_add_column = false;
        this.scale = scaleService.getScale(this.scene.width_lvl,this._scale);

        const scale_width = this.width*this.scale;
        const scale_height = this.height*this.scale;
        this.x0 = this.x - scale_width/2;
        this.x1 = this.x + scale_width/2;
        this.y0 = this.y - scale_height/2;
        this.pos_x = this.x0;
        this.pos_y = this.y0;
        this.drop_tilesprite.setX(this.x);
        this.drop_tilesprite.setY(this.y);

        this.drop_tilesprite.setDisplaySize(scale_width,scale_height);
        
        for(let match in this.battle_matchs){
            //console.log("resizy deck_math");
            this._addDeckMatch(match);
        }
    }

    refresh(){
        if(this.column>1){
            for(let deck in this.battle_matchs){
                this.battle_matchs[deck].refresh();
            }
        }
    }

    _addDeckMatch(cards){
        if(this.must_add_column){
            this.column++;
            this.must_add_column = false;
            /*for(let deck in this.battle_matchs){
                this.battle_matchs[deck].refresh();
            }*/
            //console.log("пошол во второй ряд");
            this.pos_y += this.step_y*this.scale;
        }

        if(cards in this.battle_matchs){
            this.battle_matchs[cards].onResize({x:this.pos_x,y:this.pos_y});
        }else{
            this.battle_matchs[cards.podkid.rank+cards.podkid.suit] = new DeckMatch({
                scene:this.scene,
                cards:cards,
                x: this.pos_x,
                y: this.pos_y,
                column: this.column,
                scale:this.scale
            });
            
        }

        this.pos_x+=this.step_x*this.scale;
        //console.log('this.pos_x = ', this.pos_x, ' this.x1 = ', this.x1/2);
        if(this.pos_x>=this.x1-145*this.scale){
            //console.log('должен начать координаты сначала');
            this.pos_x = this.x0+20;
            this.must_add_column = true;
            //this.temp_c_column = this.col_column;
        }
    }

    onClick(){
        //if(this.scene.select_card!=null){
        this.scene.gameLogic.dropCardOnBattle();
            //this.addCardPodkid(this.scene.select_card);
            //console.log("кликнул по полю битвы картой = ", this.scene.select_card);
        //}
    }

    customSelect(event){
        //console.log('тут select(event) = ', this);
        for(let match in this.battle_matchs){
            //console.log("match = ", match, "target = ", event.detail.match.getName());
            if(match!==event.detail.match.getName()){
                this.battle_matchs[match].select(false);
            }
        }
        //console.log("event = " , event);
    }

    isDeckHasCard(card){
        for(let match in this.battle_matchs){
            if(this.battle_matchs[match].isDeckHasCard(card)){
                return true;
            }
        }
        return false;
    }

    isCanBeat({victim,killer}){
        //console.log('victim = ', victim);
        return this.battle_matchs[victim.nominal+victim.mast].isCanBeat(killer);
    }

    isComplete(){
        for(let match in this.battle_matchs){
            if(!this.battle_matchs[match].isComplete()){
                return false;
            }
        }
        return true;
    }

    delCards(){
        for(let match in this.battle_matchs){
            this.battle_matchs[match].delCards();
            delete this.battle_matchs[match];
        }
        this.must_add_column = false;
        this.column = 1;
        this.pos_x = this.x0;
        this.pos_y = 0;
    }

    getCards(){
        const cards = [];
        for(let match in this.battle_matchs){
            this.battle_matchs[match].getCards(cards);
        }
        return cards;
    }

    addCardPodkid(card){
        const temp_cards = {
            podkid:{
                nominal: card.nominal,
                mast: card.mast
            },
            beating: null
        };

        this._addDeckMatch(temp_cards);
    }

    addCardBeating(podkid,beating){
        //console.log("добавил карту биток!!!");
        this.battle_matchs[podkid.nominal+podkid.mast].addCardBeating(beating);
    }
}