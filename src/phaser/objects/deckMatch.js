import CardFace from "./cardFace";
import Deck from "./deck";
import Card from "./card";
import scaleService from "../../services/scaleService";
//import Phaser from "phaser";

export default class DeckMatch extends Deck{
    constructor({scene,column,cards,x,y,scale}){
        super(...arguments);
        //this.x = 400;
        this.y = 250 + y;
        this.pos_y = y;
        this.column = column;
        //this.width = 115 - 10;
        //this.height = 160 - 10;
        this.cont = null;
        this.card_podkid = null;
        this.card_beating = null;
        this._scale = 0.74;
        this.scale = this._scale;
        this.is_select = false;
        this.graphics = null;
        this.color = 0xffff00;
        this.render(cards);
    }

    render(cards){
        this.cont = this.scene.add.container(this.x, this.y)
        //.setSize((Card.getSizes().w)*this.scale,(Card.getSizes().h)*this.scale)
        .setDepth(1)
        .setInteractive(new Phaser.Geom.Circle(0, 0, 60), Phaser.Geom.Circle.Contains);

        this.card_podkid = this._addCard(cards.podkid);

        this.cont.add(this.card_podkid.cont)
        .on('pointerdown', this.onClick, this)
        .on('pointerover', this.onOver, this)
        .on('pointerout', this.onOut, this);

        if(cards.beating!=null){
            this.card_beating = this._addCard(cards.beating);
            this.card_beating.cont.angle = 15;

            this.cont.add(this.card_beating.cont);
        }
        //console.log("this.cont = ", this.cont.getBounds());
    }

    onResize(pos){
        this.scale = scaleService.getScale(this.scene.width_lvl,this._scale);
        this.cont.x = pos.x + (Card.getSizes().w+60*this.scale)/2;
        this.cont.y = pos.y + (Card.getSizes().h*this.scale)/2;
        //this.cont.setInteractive(new Phaser.Geom.Rectangle(0, 0, Card.getSizes().w*this.scale, Card.getSizes().h*this.scale), Phaser.Geom.Rectangle.Contains);
        //this.cont.setSize((Card.getSizes().w)*this.scale,(Card.getSizes().h)*this.scale);
        
        this.card_podkid.onResize({x:0,y:0},this.scale);
        if(this.card_beating){
            this.card_beating.onResize({x:0,y:0},this.scale);
        }
        this.cont.setInteractive(new Phaser.Geom.Circle(0, 0, 60), Phaser.Geom.Circle.Contains);
    }

    refresh(){
        /*if(this.column==1){
            this.cont.y -= 30;
            this.card_podkid.cont.y -= 30;
            if(this.card_beating!=null){
                this.card_beating.cont.y -= 30;
            }
            this.pos_y -= 30;
        }else{
            this.cont.y += 30;
            this.card_podkid.cont.y += 30;
            if(this.card_beating!=null){
                this.card_beating.cont.y += 30;
            }
            
        }*/
    }

    _addCard(card){
        return new CardFace({
            scene: this.scene,
            card: card,
            y: this.pos_y,
            scale: this.scale
        }); 
    }

    getCards(cards){
        //let cards = [];
        cards.push({
            nominal: this.card_podkid.nominal,
            mast: this.card_podkid.mast
        });
        if(this.card_beating!=null){
            cards.push({
                nominal: this.card_beating.nominal,
                mast: this.card_beating.mast
            });
        }
        return cards;
    }

    getName(){
        return this.card_podkid.texture;
    }

    delCards(){
        /*this.card_podkid.del();
        if(this.card_beating!=null){
            this.card_beating.del();
        }*/
        this.cont.destroy();
    }

    isDeckHasCard(card){
        if(this.card_podkid.nominal==card.nominal && this.card_podkid.mast==card.mast){
            return true;
        }

        if(this.card_beating!=null){
            if(this.card_beating.nominal==card.nominal && this.card_beating.mast==card.mast){
                return true;
            }
        }

        return false;
    }

    isCanBeat(killer){
        if(this.card_beating==null){
            return true;
        }
        return false;
    }

    isComplete(){
        if(this.card_beating==null){
            return false;
        }
        return true;
    }

    addCardBeating(beating){
        //console.log("должен дорисовать карту = ", beating);
        this.card_beating = this._addCard(beating);
        this.card_beating.cont.angle = 15;
        this.cont.add(this.card_beating.cont);
    }

    onOver(){
        this.select(true);
        //console.log('вошол!!');
        // this.cont.setDepth(2);
        // this.card_podkid.setOnOver();
        // //this.cont.bringToFront();
        // if(this.card_beating!=null){
        //     //this.card_podkid.cont.x -= 30*this.scale;
        //     //this.card_beating.cont.x += 30*this.scale;
        //     this.card_podkid.onResize({x:0-30*this.scale,y:0},this.scale+0.2);
        //     this.card_beating.onResize({x:0+30*this.scale,y:0},this.scale+0.2);
        //     this.card_beating.setOnOver();
            
        // }
    }

    onOut(){
        if(this.is_select){
            return;
        }
        this.select(false);
        //console.log("вышел с карты");
        /*this.cont.setDepth(1);
        if(this.card_podkid.cont.x==0){
            return;
        }
        this.card_podkid.setOnOver();
        if(this.card_beating!=null){
            //this.card_podkid.cont.x += 30*this.scale;
            //this.card_beating.cont.x -= 30*this.scale;
            this.card_podkid.onResize({x:0,y:0},this.scale);
            this.card_beating.onResize({x:0,y:0},this.scale);
            this.card_beating.setOnOver();
            
        }*/
    }

    select(bool){
        //console.log("nen!!!!!!!!!!");
        if(bool){
            //this.is_select = true;
            this.cont.setDepth(2);
            //console.log("должен покрасить card_podkid ", this.card_podkid);
            this.card_podkid.setOnOver();

            //this.cont.bringToFront();
            if(this.card_beating!=null){
                //this.card_podkid.cont.x -= 30*this.scale;
                //this.card_beating.cont.x += 30*this.scale;
                this.card_podkid.onResize({x:0-30*this.scale,y:0},this.scale+0.2,0.8);
                this.card_beating.onResize({x:0+30*this.scale,y:0},this.scale+0.2,0.8);
                //console.log("должен покрасить card_beating");
                //this.card_beating.setOnOver();
                
            }
            //console.log(this.cont.input.hitArea);
            //console.log("this.cont = ", this.cont.getBounds());
            return;
        }
        this.cont.setDepth(1);
        /*if(this.card_podkid.cont.x==0){
            return;
        }*/
        //console.log("должен должен снять краску card_podkid");
        this.card_podkid.setOnOver();
        if(this.card_beating!=null){
            //this.card_podkid.cont.x += 30*this.scale;
            //this.card_beating.cont.x -= 30*this.scale;
            this.card_podkid.onResize({x:0,y:0},this.scale);
            this.card_beating.onResize({x:0,y:0},this.scale);
            //console.log("должен должен снять краску card_beating");
            //this.card_beating.setOnOver();
            
        }
        this.is_select = false;
    }

    onClick(){
        this.is_select = true;
        //console.log("должен создать событие!!!! на = ", this.getName());
        document.dispatchEvent(new CustomEvent("custom_select", {
            detail: { match: this }
        }));
        /*if(this.scene.server.select_card==null){
            return;
        }*/
        //this.scene.gameLogic.dropCardOnBattle({nominal: this.card_podkid.nominal, mast: this.card_podkid.mast});
    }
} 