//import {getGeneral as general} from '../general';
import Animation from './animation';

export default class Card extends Animation{
    constructor({scene,card,texture,x,y,scale,to}){
        super({scene});
        this.x = x || 0;
        this.y = y || 0;
        this.to = to || {x:this.x,y:this.y};
        
        this.texture = texture;
        this.width = Card.getSizes().w;
        this.height = Card.getSizes().h;
        this.scale = scale || 1;
        this.sprite = null;
        this.renderCard();
    }

    static getSizes(){
        return {w:115,h:160};
    }

    onResize(){
        //console.log("this.scale = ", this.scale);
        this.sprite.setDisplaySize(this.width*this.scale,this.height*this.scale);
    }

    renderCard(){     
        this.sprite = this.scene.add.sprite(this.x, this.y, this.texture)
        .setDisplaySize(this.width*this.scale,this.height*this.scale);

        /*if(this.x!=this.to.x || this.y!=this.to.y){
            //console.log('this.to = ',to);
            this.setAnimMoveTo({x:this.x, y:this.y},this.to,this.sprite);
            //console.log('стартую анимацию');
        }*/
        //console.log("должен вставить карту = ", this.sprite);
    }
}