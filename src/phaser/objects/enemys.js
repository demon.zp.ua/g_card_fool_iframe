import DeckEnemy from "../objects/deckEnemy";
import store from "../../store";

export default class Enemys{
    constructor({scene,enemys}){
        this.scene = scene;
        this.deck_enemys = {};
        this.pos_enemys = [
            {proc_x:8,proc_y:50,rot:90},
            {proc_x:50,proc_y:10,rot:0},
            {proc_x:94,proc_y:50,rot:90}
        ];
        this.init(enemys);
    }

    init(enemys){
        enemys.forEach((enemy,index) => {
			this.deck_enemys[enemy.id] = new DeckEnemy({
                id: enemy.id,
                scene: this.scene,
                temp_length: enemy.temp_length,
                length: enemy.deck,
                proc_x: this._getPosX(enemys,index),
                proc_y: this._getPosY(enemys,index),
                rot:this._getRot(enemys,index)
            });
            /*if(index>=enemys.length-1){
                console.log('изменяю сторэ');
                store.commit("game/delPlayer");
                store.commit("game/changeTest", false);
                store.commit("game/setPosLabel", {target:"dwadwa",pos:{x:10,y:20}});
            }*/
		});
    }

    _getPosX(enemys,index){
        if(enemys.length>1){
            return this.pos_enemys[index].proc_x;
        }

        return this.pos_enemys[1].proc_x;
    }

    _getPosY(enemys,index){
        if(enemys.length>1){
            return this.pos_enemys[index].proc_y;
        }

        return this.pos_enemys[1].proc_y;
    }

    _getRot(enemys,index){
        if(enemys.length>1){
            return this.pos_enemys[index].rot;
        }

        return this.pos_enemys[1].rot;
    }

    onResize(){
        for(let enemy in this.deck_enemys){
            this.deck_enemys[enemy].onResize();
        }
    }

    updateEnemys(enemys){
        enemys.forEach(enemy => {
			this.deck_enemys[enemy.id].update(enemy.deck);
		});
    }

    updateEnemy(data){
        //console.log('t_id = ', data.enemy_t_id, 'deck_enemys = ', this.deck_enemys);
        this.deck_enemys[data.enemy_t_id].update(data.deck_enemy);
    }

    distributionCard(enemy){

    }

    async update(){
        for(let deck in this.deck_enemys){
            this.deck_enemys[deck].update();
        }
    }
}