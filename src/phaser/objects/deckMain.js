import CardFace from "./cardFace";
import CardShirt from "./cardShirt";
import Deck from "./deck";
import Card from "./card";
import scaleService from "../../services/scaleService";

export default class DeckMain extends Deck{
    constructor({scene,length,trump_card}){
        super(...arguments);
        this.length = length-1;
        this.trump_card = trump_card;
        this.trump_card_obj = null;
        this.cards = [];
        this._scale = 0.7;
        this.scale = this._scale;
        this.step = 1.8;
        this.render();
    }

    static getStartPosX(screan_w,scale){
        const proc_x = 92;
        return (screan_w - Card.getSizes().w*scale/2)*proc_x/100; 
    }

    static getStartPosY(screan_h,scale){
        const proc_y = 90;
        return (screan_h - Card.getSizes().h*scale/2)*proc_y/100;
    }

    render(){
        this.trump_card_obj = new CardFace({
            scene: this.scene,
            card: this.trump_card,
            x: DeckMain.getStartPosX(this.scene.width_lvl,this.scale) - 38*this.scale,
            y: DeckMain.getStartPosY(this.scene.height_lvl,this.scale),
            scale: this.scale
        });
        this.trump_card_obj.cont.angle = 90;
        if(this.length<=0){
            this.trump_card_obj.cont.angle = 0;
            this.trump_card_obj.cont.alpha = 0.6;
        }
        

        let pos_x = DeckMain.getStartPosX(this.scene.width_lvl,this.scale);
        for(let i=0;i<this.length;i++){
            this.cards.push(
                new CardShirt({
                    scene: this.scene,
                    x: pos_x,
                    y: DeckMain.getStartPosY(this.scene.height_lvl,this.scale),
                    scale: this.scale,
                    rot: 0
                })
            );
            //console.log('в колоде = ', i);
            pos_x+=this.step;
        }
    }

    onResize(){
        this.scale = scaleService.getScale(this.scene.width_lvl,this._scale);
        let pos_x = DeckMain.getStartPosX(this.scene.width_lvl,this.scale);
        const pos_y = DeckMain.getStartPosY(this.scene.height_lvl,this.scale);

        this.trump_card_obj.onResize({x:pos_x - 38*this.scale,y:pos_y},this.scale);

        for(let card in this.cards){
            this.cards[card].onResize({x:pos_x,y:pos_y},this.scale);
            pos_x+=this.step;
        }
    }

    update(deck_length){
        //console.log('this.deck_cards.length = ', this.deck_cards.length, 'deck_length = ', deck_length);
        let temp_length = this.deck_cards.length + 1 - deck_length;
        
        //this.length -= this.length - deck;
        if(this.deck_cards.length-1<temp_length){
            temp_length = this.deck_cards.length;
        }
        //console.log('должен отнять = ', temp_length);
        for(let i=this.deck_cards.length-1;i>=this.deck_cards.length-temp_length;i--){
            //console.log('удалил карту');
            this.deck_cards[i].del();
        }
        this.deck_cards.splice(deck_length-1,temp_length);
        //console.log('в колоде осталось = ', this.deck_cards.length);
        //this.length -= temp_length;

        if(this.deck_cards.length<=0){
            this.trump_card_obj.cont.angle = 0;
            this.trump_card_obj.cont.alpha = 0.6;
        }
    }
}