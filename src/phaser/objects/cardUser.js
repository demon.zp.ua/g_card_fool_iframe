import Card from './card';
//import {getGeneral as general} from '../general';

export default class CardUser extends Card{
    constructor({scene,card,x,y,scale,to}){
        arguments[0]['texture'] = card.rank+card.suit;
        super(...arguments);
        //this.origin_x = x;
        //this.origin_y = y;
        this.graphics = null;
        this.graphics_shadow = null;
        this.ridgidBody = null;
        this.is_select = false;
        this._y = y;
        if("to" in arguments[0]){
            //console.log("пишу ту!!!");
            this._y = to.y;
        }
        
        //this._on_over = false;
        this.render();
    }

    render(){
        this.cont = this.scene.add.container(this.x, this.y)
        .setDepth(2);

        this.addGraphics();

        this.ridgidBody = this.scene.add.container(0, 0)
        .setSize((this.width)*this.scale,(this.height)*this.scale)
        .setDepth(2)
        .setInteractive()
        .on('pointerover', this.onOver, this)
        .on('pointerdown', this.onClick, this)
        .on('pointerout', this.onOut, this);

        this.sprite.x = 0;
        this.sprite.y = 0;

        this.cont.add([this.graphics_shadow,this.graphics, this.sprite, this.ridgidBody]);
        if(this.x!=this.to.x || this.y!=this.to.y){
            //console.log('this.to = ',to);
            this.setAnimMoveTo({x:this.x, y:this.y},this.to,this.cont);
            //console.log('стартую анимацию');
        }
        //console.log("this.sprite = ", this.sprite);
    }

    addGraphics(){
        this.graphics = this.scene.add.graphics()
        .setAlpha(0)
        .fillStyle(0x0521af, 1)
        .fillRoundedRect(-(this.width+4)*this.scale/2, -(this.height+4)*this.scale/2, (this.width+4)*this.scale, (this.height+4)*this.scale, 12*this.scale);

        this.graphics_shadow = this.scene.add.graphics()
        .setAlpha(0.5)
        .fillStyle(0x4a4b4c, 1)
        .fillRoundedRect(-((this.width)*this.scale/2)-3, -((this.height)*this.scale/2)+3, (this.width)*this.scale, (this.height)*this.scale, 12*this.scale);
    }

    onResize(pos,scale){
        //this._on_over = false;
        this.scale = scale;
        //console.log("pos.y = ", pos.y);
        super.onResize();

        this._y = pos.y;
        
        this.ridgidBody.setSize((this.width)*this.scale,(this.height)*this.scale);
        this.graphics_shadow.destroy();
        this.graphics.destroy();

        this.addGraphics();
        this.cont.addAt(this.graphics_shadow,0);
        this.cont.addAt(this.graphics,1);
        this.setAnimMoveTo({x:this.cont.x, y:this.cont.y},pos,this.cont,this.ridgidBody);
        //this.cont.add([this.graphics_shadow,this.graphics, this.sprite, this.ridgidBody]);  
        //this.graphics_shadow.fillRoundedRect(-((this.width)*this.scale/2)-3, -((this.height)*this.scale/2)+3, (this.width)*this.scale, (this.height)*this.scale, 12*this.scale);
        //this.graphics.fillRoundedRect(-(this.width+4)*this.scale/2, -(this.height+4)*this.scale/2, (this.width+4)*this.scale, (this.height+4)*this.scale, 12*this.scale);
        //this.cont.x = pos.x;
        //this.cont.y = pos.y;
        //this.sprite.setDisplaySize(Card*this.scale,this.height*this.scale);
        //this.cont.setSize((this.width)*this.scale,(this.height)*this.scale);
    }

    setSelect(){
        this.cont.y-=30*this.scale;
        this.is_select = true;
        this.graphics.alpha = 1;
    }

    deselectCard(){
        if(this.is_select){
            this.cont.y+=30*this.scale;
            this.is_select = false;
            this.graphics.alpha = 0;
        }
    }

    onOut(){
        //if(!this.is_select){
            if(this.cont.y>=this._y-40*this.scale){
                this.cont.y = this._y;
                //return;
            }
            //this._on_over = false;
            //this.cont.y+=40*this.scale;
        //}
        
    }

    onOver(){
        
        //if(!this.is_select){
            this.cont.y-=40*this.scale;
        //}
    }

    onClick(event){
        if(!this.is_select){
            if(this.scene.selectCard(this)){
                this.setSelect();
            }
        }
    }

    del(){
        this.cont.destroy();
        this.ridgidBody.destroy();
        delete this;
    }

    update(){
        //console.log('тут!');
        this.animate(this.cont);
    }
}