import CardShirt from "./cardShirt";
import Deck from "./deck";
import Card from "./card";
import scaleService from "../../services/scaleService";
//import Phaser from "phaser";
import store from "../../store";

export default class DeckEnemy extends Deck{
    constructor({scene,length,temp_length,x,y,scale,proc_x,proc_y,rot}){
        let t_scale = 0;
        if(scale){
            t_scale = scale;
        }
        
        //arguments[0]['scale'] = this._scale - t_scale;
        super(...arguments);
        this._scale = 0.57;
        this.scale = this._scale;
        //this.scale.scale = this._scale;
        this.temp_length = 0 || temp_length;
        this.length = length;
        this._step = 40;
        this.step = this._step;
        //this._calcStep();
        //this.koef_step = 1;
        this.proc_x = proc_x;
        this.proc_y = proc_y;
        this.rot = rot;
        this.pos_x = 0;
        this.pos_y = 0;
        //console.log("this.pos_x = ", this.pos_x);
        this.cards = [];
        
        //console.log('this.scale = ', this.scale);
        this.render();
    }

    render(){
        for(let i=0;i<this.length;i++){
            this.cards.push(
                new CardShirt({
                    scene: this.scene,
                    x: this.pos_x,
                    y: this.pos_y,
                    rot:this.rot,
                    scale: this.scale
                })
            );
            //console.log('в колоде = ', i);
            if(this.rot===0){
                //console.log('должен идти по X');
                this.pos_x+=this.step*this.scale;
            }else{
                //console.log('должен идти по Y');
                this.pos_y+=this.step*this.scale;
            }
        }
    }

    _calcStep(){
        if(this.length>6){
            this.step = this._step - (this.length-6);
        }else{
            this.step = this._step;
        }
    }

    _getStartPosX(){
        if(this.rot==90){
            return this._getStartPosXVert();
        }
        return this._getStartPosXGor();
    }

    _getStartPosY(){
        if(this.rot==90){
            return this._getStartPosYVert();
        }
        return this._getStartPosYGor();
    }

    _getStartPosXVert(){
        
        //console.log("num_cards = ", num_cards);
        return (this.scene.width_lvl-(Card.getSizes().h*this.scale)/2) * this.proc_x/100;

        //return (this.scene.width_lvl-((Card.getSizes().h*this.scale-this.step*this.scale)*num_cards-this.step*this.scale)/2) * this.proc_x/100;
        //return (this.scene.width_lvl-((Card.getSizes().w*this.scale-this.step*this.scale)*num_cards-this.step*this.scale)/2) * this.proc_x/100;
    }

    _getStartPosYVert(){
        let num_cards = this.length;
        if(this.temp_length>0){
            num_cards = this.temp_length;
        }
        //console.log('this.step = ',this.step);
        return (this.scene.height_lvl-(this.step*this.scale)*num_cards) * this.proc_y/100;
    }

    _getStartPosXGor(){
        let num_cards = this.length;
        if(this.temp_length>0){
            num_cards = this.temp_length;
        }
        //console.log("num_cards = ", num_cards);
        return (this.scene.width_lvl-(this.step*this.scale)*num_cards) * this.proc_x/100;
        //return (this.scene.width_lvl-((Card.getSizes().w*this.scale-this.step*this.scale)*num_cards-this.step*this.scale)/2) * this.proc_x/100;
    }

    _getStartPosYGor(){
        return (this.scene.height_lvl-(Card.getSizes().h*this.scale/2)) * this.proc_y/100;
    }

    onResize(){
        //console.log('ресайзаю');
        this._calcStep();
        this.scale = scaleService.getScale(this.scene.width_lvl,this._scale);
        this.pos_x = this._getStartPosX();
        this.pos_y = this._getStartPosY();
        
        for(let card in this.cards){
            this.cards[card].onResize({x:this.pos_x,y:this.pos_y},this.scale);
            if(this.rot===0){
                //console.log('должен идти по X');
                this.pos_x+=this.step*this.scale;
            }else{
                //console.log('должен идти по Y');
                this.pos_y+=this.step*this.scale;
            }
        }
        let proc_x = this.proc_x-6;
        let proc_y = this.proc_y - 40;
        
        if(this.scale<this._scale){
            proc_y = this.proc_y - (40-40*this.scale);
        }
        if(this.rot===0){
            proc_x = 12;
            proc_y = 2;
        }
        
        store.commit('game/setPosLabel',{id:this.id,proc_x:proc_x,proc_y:proc_y,w:this.scene.width_lvl,h:this.scene.height_lvl,scale:this.scale});
    }

    update_cards(deck_length){
        //const deck_length = data.deck_enemy;
        if(deck_length<this.cards.length){
            console.log('this.deck_cards.length = ', this.cards.length, 'deck_length = ', deck_length);
            let temp_length = this.cards.length - deck_length;
            
            //this.length -= this.length - deck;
            if(this.cards.length-1<temp_length){
                temp_length = this.cards.length;
            }
            console.log('должен отнять = ', temp_length);
            for(let i=this.cards.length-1;i>=this.cards.length-temp_length;i--){
                console.log('удалил карту');
                this.cards[i].del();
                this.pos_x-=this.step;
            }
            this.cards.splice(deck_length,temp_length);
            console.log('в колоде осталось = ', this.cards.length);
        }else{
            let temp_length = this.cards.length - deck_length;
            console.log('должен докинуть = ', temp_length);
            for(let i=0;i<Math.abs(temp_length);i++){
                this.cards.push(
                    new CardShirt({
                        scene: this.scene,
                        x: this.pos_x,
                        y: this.y,
                        scale: this.scale
                    })
                );
                console.log('в колоде = ', i);
                this.pos_x+=this.step;
            }
        }
    }

    update(){
        for(let card in this.cards){
            this.cards[card].update();
        }
    }
}