export default class Deck{
    constructor({scene,cards,x,y,scale,id}){
        this.id = undefined||id;
        this.scene = scene;
        this.cards = {};
        this.x = x || 0;
        this.y = y || 0;
        this.scale = scale || 1;
    }
}