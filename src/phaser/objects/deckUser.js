import CardUser from "./cardUser";
import Deck from "./deck";
import Card from "./card";
import scaleService from "../../services/scaleService";
import store from "../../store";

export default class DeckUser extends Deck{
    constructor({scene,cards,temp_cards,x,y,scale}){
        super(...arguments);
        this.step = 40;
        if(temp_cards.length>0){
            this.temp_cards = temp_cards;
        }
        this.num_cards = 0 || temp_cards.length;
        this._scale = 1;
        this.scale = 1 || scale;
        this.pos_x = this._getStartPosX();
        this.y = this._getStartPosY();
        this.render(cards);
       
    }

    render(cards){
        for(let i=0;i<cards.length;i++){
            this.cards[cards[i].rank+cards[i].suit] = this._createCard(cards[i]);
            this.pos_x+=this.step;
        }
    }

    _getStartPosX(){
        const proc_x = 45;
        /*let num_cards = this.cards.length;
        if(this.temp_cards.length>0){
            num_cards = this.temp_cards.length;
        } */
        //const proc_y = 80;
        //console.log("this.num_cards = ", this.num_cards);
        return (this.scene.width_lvl-((Card.getSizes().w*this.scale-this.step*this.scale)*this.num_cards-this.step*this.scale)/2) * proc_x/100;
        //const y = this.scene.height_lvl * proc_y/100;
    }

    _getStartPosY(){
        const proc_y = 92;
        //const proc_y = 80;
        return (this.scene.height_lvl-Card.getSizes().h*this.scale/2) * proc_y/100;
        //const y = this.scene.height_lvl * proc_y/100;
    }

    _refresh(){
        this.pos_x = this.x;
        for(let card in this.cards){
            this.cards[card].cont.x = this.pos_x;
            this.cards[card].cont.y = this.y;
            this.pos_x+=this.step;
        }
    }

    _createCard(card){
        /*if(card.suit =='b' && card.rank=='9'){
            this.scale = 1;
        }*/
        return new CardUser({
            scene: this.scene,
            card: card,
            x: 725,
            y: 600/2,
            anim_scale: {from:0.8,to:1},
            to: {x:this.pos_x,y:this.y},
            scale: this.scale
        });
    }

    distributionCard(){
        if(this.temp_cards.length<=0){
            return;
        }
        const card = this.temp_cards.splice(0, 1);
        //console.log('удалил из num_cards');
        this.num_cards--;
        this.addCards(card);
        //console.log('card = ', card);
    }

    isDeckHasCard(card){
        if(this.cards.hasOwnProperty(card.rank+card.suit)){
            return true;
        }
        return false;
    }

    deselectCards(){
        for(let card in this.cards){
            this.cards[card].deselectCard();
        }
    }

    selectVCard(card){
        if(this.isDeckHasCard(card)){
            this.cards[card.rank+card.suit].onClick();
        }
        
    }

    addCards(cards){
        for(let i=0;i<cards.length;i++){
            this.cards[cards[i].rank+cards[i].suit] = this._createCard(cards[i]);
            this.pos_x+=this.step*this.scale;
            //console.log('добавил в num_cards');
            this.num_cards++;
        }
    }

    onResize(){
        this.scale = scaleService.getScale(this.scene.width_lvl,this._scale);
        this.pos_x = this._getStartPosX();
        //console.log('this.pos_x = ', this.pos_x);
        this.y = this._getStartPosY();
        for(let card in this.cards){
            //console.log("ресайзю карту Юзера");
            this.cards[card].onResize({x:this.pos_x,y:this.y},this.scale);
            this.pos_x+=this.step*this.scale;
        }
        store.commit('game/setPosLabel',{id:this.id,proc_x:2,proc_y:80,w:this.scene.width_lvl,h:this.scene.height_lvl,scale:this.scale});
    }

    delCard(card){
        this.cards[card.nominal+card.mast].del();
        delete this.cards[card.rank+card.suit];
        this.num_cards--;
        //this._refresh();
    }

    async update(){
        for(let card in this.cards){
            this.cards[card].update();
        }
    }
}