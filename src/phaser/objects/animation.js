import Phaser from "phaser";

export default class Animation{
    constructor({scene}){
        this.scene = scene;
        this.face = null;
        this.finish_anim = true;
        this.curve = null;
        this.tween = null;
        this.path = null;
        this._to = {x:0,y:0};
        this._speed = {x:0,y:0};
        this.obj = null;
        this._interactiv_obj = null;
    }

    setAnimMoveTo(from,to,obj,interactiv_obj){
        // this._to = to;
        // this.finish_anim = false;
        // const xdiff = from.x - to.x;
        // const ydiff = from.y - to.y;
        // const alfa = Math.atan2(ydiff, xdiff);
        // this._speed.x = -8 * Math.cos(alfa);
        // this._speed.y = -8 * Math.sin(alfa);
        // 
        // this._interactiv_obj = null || obj;
        // if(this._interactiv_obj){
        //     this._interactiv_obj.removeInteractive();
        // }
        this.finish_anim = false;
        this.path = { t: 0, vec: new Phaser.Math.Vector2() };
        //console.log("from = ", from);
        //console.log("to = ", to);
        //path = scene.add.path();
        this._to = to;
        this.obj = obj;
        this.curve = new Phaser.Curves.Line(new Phaser.Math.Vector2(from.x, from.y), new Phaser.Math.Vector2(to.x, to.y));

        this.scene.tweens.add({
            targets: this.path,
            t:1,
            onComplete: this.compliteAnimMoveTo,
            onCompleteScope:this,
            ease: 'Linear',       // 'Cubic', 'Elastic', 'Bounce', 'Back'
            duration: 600,
            repeat: 0,            // -1: infinity
            yoyo: false
        });

        this._interactiv_obj = null || interactiv_obj;
        if(this._interactiv_obj){
            this._interactiv_obj.removeInteractive();
        }
        
        //console.log('this._speed = ',this._speed);
    }

    addFace(face){

    }

    compliteAnimMoveTo(){
        //console.log("this.scene.getAllTweens() = ", this.finish_anim);
        this.finish_anim = true;
        this.curve = null;
        //console.log("this.scene.getAllTweens() = ", this.scene);
        this.scene.tweens.killTweensOf(this.path);
        //this.tween = null;
        this.obj.x = this._to.x;
        this.obj.y = this._to.y;
        this.path = null;
        if(this._interactiv_obj){
            this._interactiv_obj.setInteractive();
            this._interactiv_obj = null;
        }
    }

    moveTo(obj){
        this.curve.getPoint(this.path.t, this.path.vec);
        obj.x = this.path.vec.x;
        obj.y = this.path.vec.y;
        /*let differenceX = obj.x - this._to.x;
		let differenceY = obj.y - this._to.y;
				
		if(Math.abs(differenceX)<6 && Math.abs(differenceY)<6){
            this.finish_anim = true;
            obj.x = this._to.x;
            obj.y = this._to.y;
            if(this._interactiv_obj){
                this._interactiv_obj.setInteractive();
            }
            //console.log('кончил анимацию');
		}
        
        obj.x += this._speed.x;
        obj.y += this._speed.y;*/
        
        //console.log('здесь!');
    }

    shirtToFace(face){

    }

    faseToShirt(){

    }

    animate(cont){
        if(!this.finish_anim){
            //console.log('тут!');
            this.moveTo(cont);
            /*this.cont.x += pos.x;
            this.cont.y += pos.y;*/
        }
    }
}