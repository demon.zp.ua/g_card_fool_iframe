import Card from './card';

export default class CardFace extends Card{
    constructor({scene,card,x,y,scale}){
        arguments[0]['texture'] = card.rank+card.suit;
        super(...arguments);
        this.graphics = null;
        this.graphics_shadow = null;
        this.render();
    }

    render(){
        this.cont = this.scene.add.container(this.x, this.y);

        this.addGraphics();

        this.sprite.x = 0;
        this.sprite.y = 0;
        this.cont.add([this.graphics_shadow,this.graphics, this.sprite]);
        //console.log("this.sprite = ", this.sprite);
    }

    addGraphics(alpha){
        let t_a = 0;
        if(alpha){
            t_a = alpha;
        }
        this.graphics = this.scene.add.graphics()
        .setAlpha(t_a)
        .fillStyle(0x0521af, 1)
        .fillRoundedRect(-(this.width+5)*this.scale/2, -(this.height+5)*this.scale/2, (this.width+5)*this.scale, (this.height+5)*this.scale, 12*this.scale);

        this.graphics_shadow = this.scene.add.graphics()
        .setAlpha(0.5)
        .fillStyle(0x4a4b4c, 1)
        .fillRoundedRect(-((this.width)*this.scale/2)-3, -((this.height)*this.scale/2)+3, (this.width)*this.scale, (this.height)*this.scale, 12*this.scale);
    }

    onResize(pos,scale,alpha){
        this.scale = scale;
        this.cont.x = pos.x;
        this.cont.y = pos.y;
        super.onResize();

        this.graphics_shadow.destroy();
        this.graphics.destroy();

        this.addGraphics(alpha);

        this.cont.addAt(this.graphics_shadow,0);
        this.cont.addAt(this.graphics,1);
    }

    setOnOver(){
        //console.log("до this.graphics.alpha = ", this.graphics.alpha);
        this.graphics.alpha = this.graphics.alpha==0 ? 0.8 : 0; 
        //console.log("после this.graphics.alpha = ", this.graphics.alpha);
    }

    del(){
        this.cont.destroy();
        delete this;
    }
}