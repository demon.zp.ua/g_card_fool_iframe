// Класс игровой сцены содержин визуальную логику игры
import Phaser from "phaser";
import DeckUser from "../objects/deckUser";
import DeckMain from "../objects/deckMain";
import DeckBattle from "../objects/deckBattle";
import Enemys from "../objects/enemys";
import DistributionService from "../services/distributionService";
import store from "../../store";
import scale from "../../services/scaleService";

export default class GameScene extends Phaser.Scene{
	constructor(config){
		super({key:"GameScene"});
        //super(config);
        this._d_width = 888;
        this._d_height = 600;
		this.width_lvl = 400;
        this.height_lvl = 280;
        //this.scale = 1;
        this.init_num_cards = 0;
        this.deck_user = null;
        this.enemys = null;
        this.deck_battle = null;
        this.deck_main = null;
        this._scale_player = 1;
        this._scale_enemy = 0.6;
        this._scale_main_deck = 0.8;
        this._scale_battle_deck = 0.86;
	}

// Создает класс Игровой логики клиентской части
	init(){
		
		//this.gameLogic = new GameLogic(this);
		//vue.$store.commit('socket/setGameLogic', this);
	}

	create(){
		console.log("создал сцену Гаме");
		
		this.tilesprite = this.add.tileSprite(this.width_lvl/2, this.height_lvl/2, this.width_lvl, this.height_lvl, 'green_field');
        store.state.game.game_render = this;
        this.render();
		//let deckUser = new DeckUser(this,[{nominal:6, mast:'c'},{nominal:8, mast:'b'},{nominal:13, mast:'k'}]);
	}

/* 
Создает сцену для визуализации виртуального сервера затычки пока нету реального Бэка
и отдает ее под управление класса затычки
*/

// Создает визуальные игровые колоды	
	render(game){
        this.deck_main = new DeckMain({
			scene: this,
			length: store.state.game.game.main_deck,
			trump_card: store.state.game.game.tramp_card
		});
        //console.log('game.render()!~!');
        this.deck_user = new DeckUser({
            id: store.state.game.game.user_id,
			scene: this,
            cards: [],
            temp_cards:store.state.game.game.user_cards,
			x: 0,
			y: 0
        });
        /*store.state.game.game.enemys.forEach(enemy => {
            enemy['temp_length'] = enemy.cards;
        });*/
        this.enemys = new Enemys({
			scene: this,
            enemys: store.state.game.game.enemys
        });
        
        this.deck_battle = new DeckBattle({
			scene: this,
			cards: store.state.game.game.deck_battle
		});

        const distributionService = new DistributionService({scene:this,player:this.deck_user,enemys:this.enemys});
        //this.onResize(1000,600);
        /*const timedEvent = this.time.addEvent({ 
            delay: 1000,
            repeat:summ,
            callback: addCard(summ), 
            callbackScope: this });*/
		/*this.gameLogic.whose_turn = response.whose_turn;

		this.deck_battle = new DeckBattle({
			scene: this,
			cards: response.deck_battle
		});
		
		this.deck_user = new DeckUser({
			scene: this,
			//cards: [{mast:'k',nominal:6},{mast:'c',nominal:12}],
			cards: response.deck_user,
			x: 200,
			y: 500
		});

		this.enemys = new Enemys({
			scene: this,
			enemys: response.enemys
		});
		

		this.deck_main = new DeckMain({
			scene: this,
			length: response.deck_main,
			trump_card: response.kozir,
			x: 725,
			y: 600/2,
			scale: 0.8
		});*/

		//this.interfaceC.onFunc('appButtons','show');
		
    }

	isDeckHasCard(deck, card){
		return this[deck].isDeckHasCard(card);
	}

	gameOwer(winner){
		console.log('Игра окончена победил = ', winner);
    }

	onResize(w,h){
        this.width_lvl = w;
        this.height_lvl = h;
        this.scale.resize(w, h);
        this.tilesprite.setX(w/2);
        this.tilesprite.setY(h/2);
        this.tilesprite.setDisplaySize(w,h);
        this.deck_main.onResize();
        this.deck_battle.onResize();
        //console.log("ресайзю Узера");
        this.deck_user.onResize();
        this.enemys.onResize();
        this.scale.updateBounds();
        //win_size = scale.getSize(w,h);
		//this.scale.updateBounds();
		//this.cameras.resetAll();
		//this.cameras.main.clearRenderToTexture();
		//console.log('должен this.deck_user.onResize();');
		//this.deck_user.onResize();
	}

	update(){
		for(let i=0;i<store.state.game.action_queue.length;i++){
            const action = store.state.game.action_queue[i];
            switch (action.action) {
                case "resize":
                    //console.log("вижу задачу ресайз");
                    this.onResize(action.data.w,action.data.h);
                    break;
                default:
                    console.error('Sorry, we are out of ' + action.action + '.');
            }
            //console.log("должен уждалить = ", i);
            store.state.game.action_queue.splice(i,1);
            //console.log('store.state.game.action_queue = ', store.state.game.action_queue);
        }
        
        
        if(this.deck_user){
            this.deck_user.update();
        }
        
        if(this.enemys){
			this.enemys.update();
		}
	}
}