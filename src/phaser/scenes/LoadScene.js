/*
Класс загрузки ассетов для игры, картинок карт использует класс general
где генерируються имена карт для типа удобной загрузки картинок через цикл
*/

import Phaser from 'phaser';
import store from '../../store';
//import {vue} from '../../main';

export default class LoadScene extends Phaser.Scene{
	constructor(config){
        //super(config);
		super({key:"LoadScene"});
    }

	preload(){
        const cards = store.state.general.logic_cards;
        cards.forEach(card => {
            this.load.image(`${card.rank}${card.suit}`, `./assets/cards/${card.rank}${card.suit}.png`);
        });
		
        this.load.image('shirt1', './assets/cards/rubaha1.png');
        this.load.image('green_field', './assets/green_field.jpg');
		this.load.image('battle_field', './assets/battle_field.jpg');
		
		const loadingBar = this.add.graphics({
			fillStyle:{
				color:0xffffff
			}
		});

		this.load.on("progress",(percent)=>{
			loadingBar.fillRect(0,this.game.renderer.height/2,this.game.renderer.width*percent,50);
		});
		
		this.load.on("complete", ()=>{
            console.log("загрузка завершена!!!!!!!!!!!!!!");
        })
	}

// после загрузки всех ассетов стартует GameScene
	create(data){
		this.scene.start("GameScene");
		//this.scene.start("ServerScene");
	}
}