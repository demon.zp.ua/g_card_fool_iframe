import Vue from 'vue'
import Vuex from 'vuex'
import general from './store/general'
import game from './store/game'

Vue.use(Vuex)

export default new Vuex.Store({
    modules:{
        general,
        game
    }
})
